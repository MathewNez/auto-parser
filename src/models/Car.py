from typing import Optional


class Car:
    name: Optional[str]
    year: Optional[str]
    image_url: Optional[str]
    special_info: Optional[str]
    price: Optional[str]
    ad_link: Optional[str]
    geo: Optional[str]
    engine_capacity: Optional[str]
    gearbox: Optional[str]
    horsepower: Optional[str]
    mileage: Optional[str]
    body_type: Optional[str]
    drive_unit: Optional[str]
    fuel: Optional[str]
    color: Optional[str]
