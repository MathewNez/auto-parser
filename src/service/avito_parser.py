import re
from typing import List, Union, Tuple
from bs4 import BeautifulSoup

from src.service.parsing_utils import get_page_source, get_text_or_none, get_attr_or_none
from src.models.Car import Car


def get_cars(page_source: str) -> List[BeautifulSoup]:
    """
    Ищет все "элементы-машины"(объявления) на странице Авито.

    :param page_source: Исходный код страницы.
    :return: Список "супов-машин"(супов-объявлений).
    """
    soup: BeautifulSoup = BeautifulSoup(page_source, 'html.parser')
    cars: List[BeautifulSoup] = soup.findAll('div', attrs={'data-marker': 'item'})
    return cars


def get_image(car: BeautifulSoup) -> Union[str, None]:
    """
    Достаёт URL картинки объявления.
    Сначала ищет просто тег img, если не находит, пытается вытащить
    из атрибута одного из составляющих слайдера.

    :param car: Объявление-суп.
    :return: URL картинки объявления или None.
    """
    image_url: BeautifulSoup = car.find('img', attrs={'itemprop': 'image'})
    if not image_url:
        image_url: BeautifulSoup = car.find('li', attrs={'class': re.compile(r'^photo-')})
        if not image_url:
            return None
        image_url: str = image_url.attrs['data-marker'].replace('slider-image/image-', '')
    else:
        image_url: str = image_url.attrs['src']

    return image_url


def get_name_n_year(car: BeautifulSoup) -> Tuple[Union[str, None], Union[str, None]]:
    """
    Позволяет получить название(марка и модель) и год автомобиля.
    Поскольку на Авито они хранятся в одной строке, метод пытается разделить эти значения.

    :param car: Объявление-суп.
    :return: Кортеж вида (название, год). В случае неудач элементы могут быть None.
    """
    title: str = get_text_or_none(car, tag='h3', attrs={'itemprop': 'name'})
    if not title:
        return None, None
    name_n_year: List[str] = title.split(', ')
    try:
        name, year = name_n_year[0], name_n_year[1]
    except IndexError:
        return title, None

    return name, year


def get_params(car: BeautifulSoup) -> Union[List[str], None]:
    """
    Получает и разделяет параметры автомобиля.

    :param car: Объявление-суп.
    :return: Список параметров:
    0 - пробег,
    1 - набор основных параметров,
    2 - тип кузова,
    3 - привод,
    4 - топливо,
    5 - спец. информация (вроде, там может быть только "Битый"),
    6 - основной параметр: объём двигателя,
    7 - основной параметр: коробка,
    8 - основной параметр: лошадиные силы.
    """
    params: str = get_text_or_none(car, tag='div', attrs={'data-marker': 'item-specific-params'})
    if not params:
        return None

    params_arr: list = params.split(', ')
    special: None = None
    if 'км' not in params_arr[0]:
        if 'л.с.' in params_arr[0]:
            params_arr.insert(0, '0 км (новый)')
        else:
            special: str = params_arr[0]
            params_arr: list = params_arr[1:]
    params_arr.append(special)
    main_params: list = params_arr[1].split(' ')
    params_arr.extend(main_params)

    return params_arr


def get_link(car: BeautifulSoup) -> Union[str, None]:
    """
    Собирает ссылку на объявление.
    Вытаскивает из тайтла href и добавляет к ссылке начало.

    :param car: Объявление-суп.
    :return: Ссылка на объявление.
    """
    link_end: str = get_attr_or_none(car, tag='a', attrs={'data-marker': 'item-title'}, attr='href')
    link = f'https://avito.ru{link_end}'
    return link


def parse_car(car: BeautifulSoup) -> Car:
    """
    Собирает всю информацию о машине в одну модель.

    :param car: Объявление-суп.
    :return: Машина как класс.
    """
    parsed = Car()
    parsed.image_url = get_image(car)
    parsed.name, parsed.year = get_name_n_year(car)
    parsed.price = get_text_or_none(car, tag='span', attrs={'data-marker': 'item-price'})
    params = get_params(car)
    try:
        parsed.mileage = params[0]
        parsed.body_type = params[2]
        parsed.drive_unit = params[3]
        parsed.fuel = params[4]
        parsed.special_info = params[5]
        parsed.engine_capacity = params[6]
        parsed.gearbox = params[7]
        parsed.horsepower = params[8]
    except (IndexError, TypeError):
        pass

    parsed.geo = get_text_or_none(car, tag='div', attrs={'class': re.compile(r'^geo-')})
    parsed.ad_link = get_link(car)

    return parsed


def main():
    avito_link = 'https://www.avito.ru/ekaterinburg/avtomobili?cd=1&radius=200&s=104'
    page_source = get_page_source(avito_link)
    cars = get_cars(page_source)
    parsed_cars = []
    for car in cars:
        parsed_cars.append(parse_car(car))

    for car in parsed_cars:
        print(vars(car))


if __name__ == '__main__':
    main()
