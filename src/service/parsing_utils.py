import os

from typing import Union

from bs4 import BeautifulSoup
from selenium import webdriver
from dotenv import load_dotenv

load_dotenv()

gecko_path = os.getenv('GECKO_PATH')


def get_page_source(link: str, wd_path: str = gecko_path) -> str:
    """
    Позволяет получить исходный код страницы.
    Запускает вебдрайвер, получает страницу, останавливает драйвер.

    :param link: Ссылка, по которой находится нужная страница.
    :param wd_path: Путь до вебдрайвера Gecko(Firefox).
    :return: Строка, содержащая весь исходный html-код страницы.
    """
    driver = webdriver.Firefox(wd_path)
    driver.get(link)
    content: str = driver.page_source
    driver.quit()
    return content


def get_text_or_none(car: BeautifulSoup, tag: str, attrs: dict) -> Union[str, None]:
    """
    Позволяет найти в супе нужный тег и вернуть с него текст.
    В случае, когда поиск не увенчался успехом, вернёт None.

    :param car: Объект супа.
    :param tag: html-тег, который нужно найти
    :param attrs: Атрибуты, которые должны быть у тега.
    :return: Текст внутри тега или None.
    """
    result: BeautifulSoup = car.find(tag, attrs=attrs)
    if not result:
        return None

    return result.text


def get_attr_or_none(car: BeautifulSoup, tag: str, attrs: dict, attr: str) -> Union[str, None]:
    """
    Позволяет найти в супе нужный тег и вернуть с него атрибут.
    В случае, когда поиск не увенчался успехом, вернёт None.

    :param car: Объект супа.
    :param tag: html-тег, который нужно найти
    :param attrs: Атрибуты, которые должны быть у тега.
    :param attr: Какой атрибут необходимо вернуть.
    :return: Значение атрибута или None.
    """
    result: BeautifulSoup = car.find(tag, attrs=attrs)
    if not result:
        return None

    return result.attrs[attr]
