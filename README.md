# Auto-parser

Auto-parser is a Python project which allows you to get parsed info about cars
from such sites as [avito.ru](https:://avito.ru) and [auto.ru](https:://auto.ru)
as fast as possible. Default notifications from these sites are coming to you
only after 3-15 minutes when ad was published. Our service scans for new ads
every `<time_interval>`.

## Project structure
This project has layered architecture. Here are the layers:
```
auto-parser
L___src
    L___api -- api controllers
    L___models -- classes describing entities
    L___service -- all of the logic
    L___tests -- unit tests
```

## Stack

### Current:

* beautifulsoup4
* selenium
* pipenv

### Planned:
* asyncio or smth
* pytest
* etc.

## Developing

1. Clone project
```shell
git clone <url>
```
2. Go to project root folder
```shell
cd auto-parser
```
3. Install pipenv globally
```shell
pip3 install pipenv
```
4. If needed, create `.venv` folder in root project folder
```shell
mkdir .venv
```
5. `Pipenv` can automatically create venv and install all required dependencies, to do this, just type
```shell
python3 -m pipenv shell
```
If you did step 3, virtual environment will be created in `.venv` folder.
In other case, `pipenv` will choose folder for `venv` automatically.
6. Create a `.env` file
```shell
touch .env
```
7. `.env` file should contain all variables from `.env.example` with your values.
For now, you should specify path to your gecko driver in `.env`.

### Package managing

#### To install package, type:
```shell
pipenv install <pkg_name>
```
You may also add `--dev` arguement to demacrate packages needed only for developing.

## Plans for the future
* add auto.ru
* add proxy changing in order to avoid bans
* make service scan sites every `<time_interval>`

## Deploying

This section is in progress...